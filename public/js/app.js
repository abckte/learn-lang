window.addEventListener("keypress", (event) => {
    console.dir(event);
    switch (event.code) {
        case "KeyA":
            document.getElementById('responseOkBtn').click();
            break;
        case "KeyD":
            document.getElementById('responseNokBtn').click();
            break;
        case "Space":
        case "KeyW":
            document.getElementById('question').click();
            break;
    }
    return event;
})
