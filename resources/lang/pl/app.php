<?php

return [
    'name' => 'apka do nauki języka',
    'uploadFile' => 'wgraj plik',
    'send' => 'ślij',
    'answer' => 'odpowiedź',
    'ok' => 'znam',
    'nok' => 'nie rozumiem',
    'nextLevel' => 'Następny level',
    'quiz' => 'quiz',
    'addingBook' => 'dodawanie książki',
    'welcomeOnLevel' => 'witaj na poziomie',
];
