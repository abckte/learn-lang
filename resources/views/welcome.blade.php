<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <title>{{__('app.name')}}</title>
</head>
<body>
<div>
    {{__('app.name')}}
    <a href="{{route("addingBook")}}">{{__('app.addingBook')}}</a>
    <br />
    <a href="{{route("quiz")}}">{{__('app.quiz')}}</a>
</div>
</body>
</html>
