<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="utf-8">
    <title>Laravel</title>
</head>
<body>
{{ __('app.name') }}
<br/>
@if ($message = Session::get('success'))
    <div class="alert alert-success alert-block">
        <strong>{{ $message }}</strong>
    </div>
@endif
@if ($message = Session::get('crash'))
    <div class="alert alert-success alert-block">
        <strong>{{ $message }}</strong>
    </div>
@endif

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<form action="#" method="post" enctype="multipart/form-data">
    @csrf
    <input type="file" name="book" value="{{__('app.uploadFile')}}">
    <input type="submit" value="{{__('app.send')}}">
</form>
</body>
</html>
