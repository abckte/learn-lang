<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="utf-8">
    <title>{{ __('app.name') }}</title>
    <link rel="stylesheet" href="/css/app.css">
    <script src="/js/app.js">
    </script>
</head>
<body>
<div class="container">
    <div class="header">
        <div class="settings">
            ֍
            <div class="list">
                <a href="{{route("quiz")}}?lvl={{$lvl['next']}}">{{__('app.nextLevel')}}</a>
            </div>
        </div>
        {{ __('app.name') }}<br/>
        @if(Session::get('lvl') !== null)
            {{ __('app.welcomeOnLevel') }} {{Session::get('lvl')}}
        @endisset
    </div>

    <div class="content">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="question">
            <div id="question"
                 onclick="document.getElementById('answer').innerHTML = '{{$word->getAnswer()}}'; document.getElementById('answer').classList.add('show');">
                {{$word->word}}
            </div>

            <div id="answer" class="answer"></div>
        </div>
        <div class="response">
            <a class="ok" id="responseOkBtn" href="{{route("quiz")}}/ok">{{__('app.ok')}} <small>(A)</small></a>
            <a class="nok" id="responseNokBtn" href="{{route("quiz")}}/nok">{{__('app.nok')}} <small>(D)</small></a>
        </div>
    </div>
</div>
</body>
</html>
