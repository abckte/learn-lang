# learn lang app
## about
it's app to help author of this software improve language.
it's simple flashcard app.

## usage
### auth
first enter to site have to be done with key parameter. Key is specifed in `TRUST_KEYS` in env.

Example usage:
`https://example.com?key=secret_password`
### logged
add book in any language in epub format.
next open quiz and learn, learn and learn.

On lower levels you see very often usage words. Next more strange.

this app using google translator, so it can detect input language.
## installation

### requirements
 - php ^7.4
 - composer
 - network access to google api from server (`https://translate.googleapis.com`)

### installation commands
first download this git.
next in destination directory type:

> composer install

in case errors please install missing php extensions.

> php artisan key:generate

last think is set up env

in `.env` file put missing values:
- GOOGLE_TRANSLATE_API_KEY
- TRUST_KEYS

#### GOOGLE_TRANSLATE_API_KEY
https://cloud.google.com/docs/authentication/api-keys

#### TRUST_KEYS
because it's really quick and only for me project, so have special authorization. so put there your preferred password.
Remember this parameter in the future will me send to app by not secure method. So please don't expect high security in this app.
