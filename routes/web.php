<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::fallback(function () {
    return \Illuminate\Support\Facades\Redirect::route("welcome");
});
Route::get('/', [\App\Http\Controllers\HomeAuthorizedController::class, "welcome"])->name("welcome");
Route::get('/book', [\App\Http\Controllers\BookAuthorizedController::class, "addBookView"])->name("addingBook");
Route::post('/book', [\App\Http\Controllers\BookAuthorizedController::class, "addBook"]);
Route::get('/quiz', [\App\Http\Controllers\QuizAuthorizedController::class, "getQuestion"])->name("quiz");
Route::any('/quiz/ok', [\App\Http\Controllers\QuizAuthorizedController::class, "getQuestion"]);
Route::any('/quiz/nok', [\App\Http\Controllers\QuizAuthorizedController::class, "getQuestion"]);
Route::any('/system/selfUpdate', [\App\Http\Controllers\UnAuthSystemController::class, "selfUpdate"]);
