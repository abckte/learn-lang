<?php

namespace App\Services;

use App\Models\Lang;
use App\Models\Word;
use Google\Cloud\Translate\V2\TranslateClient;

class TranslateService
{
    /**
     * Translate word or sentence to dedicated language
     * current available only autodetect language
     *
     * @param Word $word
     * @param Lang|null $targetLang
     * @param Lang|null $sourceLang current only autodetect
     * @return string
     */
    public static function translate(Word $word, Lang $targetLang = null, Lang $sourceLang = null): string
    {
        if(env("APP_DEBUG") == "true"){
            return "test";
        }
        $key = env("GOOGLE_TRANSLATE_API_KEY");

        if ($targetLang === null) {
            $targetLang = new Lang('pl');
        }
        $translationClient = new TranslateClient([
            'key' => $key,
            'target' => $targetLang->getShortName()
        ]);
        $response = $translationClient->translate($word->word);
        return $response['text'];
    }
}
