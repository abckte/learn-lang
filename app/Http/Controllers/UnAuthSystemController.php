<?php

namespace App\Http\Controllers;

class UnAuthSystemController extends UnAuthorizedController
{
    public function selfUpdate()
    {
        $last_line = system('cd /srv/lern-lang/ ; git pull', $retval);
        return response()->json(['success' => true, 'return' => $retval, 'last_line' => $last_line]);
    }
}
