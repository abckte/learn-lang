<?php


namespace App\Http\Controllers;


use App\Models\Word;
use Illuminate\Http\Request;
use lywzx\epub\EpubParser;

class BookAuthorizedController extends AuthorizedController
{
    public function addBookView()
    {
        return view('addBook');
    }

    public function addBook(Request $request)
    {
        $request->validate([
            'book' => 'required|mimes:epub,xlx,csv|max:2048',
        ]);
//        var_dump($request->file("book")->getPathname());
        try {
            $epubParser = new EpubParser($request->file("book")->getPathname());
            $epubParser->parse();
            $chapters = array_keys($epubParser->getManifestByType('application/xhtml+xml'));
//            var_dump($chapters);
            $wordCount = 0;
            $newWordCount = 0;
            foreach ($chapters as $chapterTitle) {
                $chapter = $epubParser->getChapter($chapterTitle);
                $chapterRaw = strip_tags($chapter);
                $wordList = $this->multiexplode(array(" ", ",", ".", "|", ":", ' ', "\n", "\t", "'", '"', ';', '<', '>', '?', '-', '–', '—', '_', '+', '=', '[', ']', '{', '}',
                    '|', '/', '\\', '(', ')', '!','@','#','$','%','^','&','*', '„', '”'), $chapterRaw);
                foreach ($wordList as $word) {
                    if ($word != "") {
//                        var_dump($word);
//                        echo (ord($word[0])) . " \t ". strtolower($word) . "<br />";
//                        echo strtolower($word) . "<br />";
                        $word = Word::firstOrCreate([
                            'word' => strtolower($word)
                        ]);
                        $word->count++;
                        $word->save();
                        $wordCount++;
                    }
                }
            }
            return back()
                ->with('success', "done ".count($chapters)." chapters witch ".$wordCount." word(s)");
        } catch (\Exception $e) {
            return back()
                ->with('crash', $e->getMessage());
        }

//        $fileName = time().'.'.$request->file->extension();
//
//        $request->file->move(public_path('uploads'), $fileName);
//
//        var_dump($request->file);
    }

    private function multiexplode($delimiters, $string)
    {

        $ready = str_replace($delimiters, $delimiters[0], $string);
        $launch = explode($delimiters[0], $ready);
        return $launch;
    }
}
