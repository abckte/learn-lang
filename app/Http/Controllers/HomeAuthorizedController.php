<?php


namespace App\Http\Controllers;


use App\Models\Word;
use Illuminate\Http\Request;
use lywzx\epub\EpubParser;

class HomeAuthorizedController extends AuthorizedController
{
    public function welcome()
    {
        return view('welcome');
    }
}
