<?php

namespace App\Http\Controllers;

use App\Models\Word;
use Illuminate\Http\Request;

class QuizAuthorizedController extends AuthorizedController
{
    public function getQuestion(Request $request)
    {

        $level = ($request->input("lvl") != null) ? $request->input("lvl") : null;
        if ($level === null) {
            $level = $request->session()->get("lvl", 1);
        } else {
            $request->session()->put("lvl", $level);
        }
        $wordsInLevel = 100;
        $numOfWord = rand(0, $wordsInLevel);

        return view("quiz.question", [
            "word" => Word::orderBy("count", "DESC")->skip(($level * $wordsInLevel) + $numOfWord)->first(),
            "lvl" => [
                "current" => $level,
                "next" => ((int)$level) + 1
            ]
        ]);
    }
}
