<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Redirect;

class AuthorizedController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if ($request->session()->get('authorized', false) !== true) {
                $keys = explode(";", env("TRUST_KEYS"));
                if (in_array($request->input('key'), $keys)) {
                    $request->session()->put('authorized', true);
                } else {
                    Redirect::to('https://pl.wikipedia.org/wiki/Blue_Screen_of_Death#/media/Plik:Windows_9X_BSOD.png')->send();
                    abort(401);
                }
            }

            return $next($request);
        });
    }
}
