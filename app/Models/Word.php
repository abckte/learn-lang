<?php

namespace App\Models;

use App\Services\TranslateService;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static $this firstOrCreate(array $array)
 * @method static $this orderBy(string $column, string $direction = "ASC")
 * @method static $this skip(float|int $param)
 * @method static $this take(int $wordsInLevel)
 * @method static $this first()
 * @property string $word
 */
class Word extends Model
{
    use HasFactory;

    protected $fillable = ['word'];

    public function getAnswer(): string
    {
        return TranslateService::translate($this);
    }
}
