<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 *  model to represent source or destination language
 */
class Lang
{
    private static $langArr = [
        'pl' => [
            'shortName' => 'pl',
            'shortLongEnName' => 'Polish',
            'shortLongName' => 'Polski',
        ],
        'en' => [
            'shortName' => 'en',
            'shortLongEnName' => 'English',
            'shortLongName' => 'English',
        ]
    ];
    private $selectedLang;
    public function __construct(string $lang)
    {
        $this->selectedLang = self::$langArr[$lang];
    }

    public function getShortName(): string{
        return $this->selectedLang['shortName'];
    }

}
